import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Calculatrice from './components/Calculatrice';


export const App = () => {
  return (
    <View style={{flex: 1}}>
      <Calculatrice></Calculatrice>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({});
