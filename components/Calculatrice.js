import React, {Component, useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';

export const Calculatrice = () => {
  const [operandePremier, setOperandePremier] = useState("");
  const [operandeSecond, setOperandeSecond] = useState("");
  const [resultat, setResultat] = useState("Resultat");

  const plus = () => {
    let operandePremierInput = parseFloat(setOperandePremier(operandePremierInput));
    let operandeSecondInput = parseFloat(setOperandeSecond(operandeSecondInput));
    console.log(operandePremierInput)
    console.log(operandeSecondInput)
    setResultat(operandePremierInput + operandeSecondInput)
  };
  const minus = () => {
    let operandePremierInput = parseFloat(setOperandePremier(operandePremierInput));
    let operandeSecondInput = parseFloat(setOperandeSecond(operandeSecondInput));
    setResultat(operandePremierInput - operandeSecondInput)
  };
  const divide = () => {
    let operandePremierInput = parseFloat(setOperandePremier(operandePremierInput));
    let operandeSecondInput = parseFloat(setOperandeSecond(operandeSecondInput));
    setResultat(operandePremierInput / operandeSecondInput)
  };
  const multiply = () => {
    let operandePremierInput = parseFloat(setOperandePremier(operandePremierInput));
    let operandeSecondInput = parseFloat(setOperandeSecond(operandeSecondInput));
    setResultat(operandePremierInput * operandeSecondInput)
  };

    return (
      <View style={styles.container}>
        <View style={styles.resultContainer}>
          <Text style={{color: 'green', fontSize: 21, paddingLeft: 10}}>
            {resultat}
          </Text>
        </View>
        <View style={styles.firstInput}>
          <TextInput
            onChangeText={operandePremier => setOperandePremier(operandePremier)}
            value={operandePremier}
            style={{color: 'green', fontSize: 21, paddingLeft: 10}}
            placeholder="Opérande A"
            placeholderTextColor="green"
            keyboardAppearance="dark"
            keyboardType="numeric"
          />
        </View>
        <View style={styles.secondInput}>
          <TextInput
            onChangeText={operandeSecond => setOperandeSecond(operandeSecond)}
            value={operandeSecond}
            style={{color: 'green', fontSize: 21, paddingLeft: 10}}
            placeholder="Opérande B"
            placeholderTextColor="green"
            keyboardAppearance="dark"
            keyboardType="numeric"
          />
        </View>

        <View>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={plus}>
              <View style={styles.plusButton}>
                <Text style={styles.buttonText}> + </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={minus}>
              <View style={styles.minusButton}>
                <Text style={styles.buttonText}> - </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={divide}>
              <View style={styles.divideButton}>
                <Text style={styles.buttonText}> / </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={multiply}>
              <View style={styles.multiplyButton}>
                <Text style={styles.buttonText}> * </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

export default Calculatrice

const {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
  },
  resultContainer: {
    height: height * 0.1,
    width: width,
    backgroundColor: 'black',
    justifyContent: 'center',
  },
  firstInput: {
    height: height * 0.1,
    width: width,
    backgroundColor: 'black',
    justifyContent: 'center',
    marginTop: 10,
  },
  secondInput: {
    height: height * 0.1,
    width: width,
    backgroundColor: 'black',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: height * 0.02,
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height * 0.25,
  },
  plusButton: {
    width: width / 2,
    height: height * 0.25,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
  },
  minusButton: {
    width: width / 2,
    height: height * 0.25,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  divideButton: {
    width: width / 2,
    height: height * 0.25,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  multiplyButton: {
    width: width / 2,
    height: height * 0.25,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 50,
    color: 'white',
  },
});
